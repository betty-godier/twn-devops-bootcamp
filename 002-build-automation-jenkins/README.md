# DevOps Practioner

## Portfolio

## Demonstrating CI/CI build automation with Jenkins

Simply demo of build automation and continuous integration
Checkout Git Repo -> Increment version -> Run Tests -> Build Jar File -> Build Image -> Push to Private Repo

## Author

Betty Godier
[Linkedin Profile](https://www.linkedin.com/in/betty-godier/)

# Technologies used
Gitlab, Jenkins
Gradle, aws Ec2
Docker


## Install Jenkins on DigitalOCean
droplet on DigitalOcean with configuration for firewall port 8080 for Jenkins

root@ubuntu-s-2vcpu-4gb-ams3-01:~# docker run -p 8080:8080 -p 50000:50000 \
> -d \
> -v jenkins_home:/var/jenkins_home \
> jenkins/jenkins:lts

We can access Jenkins from the browser : http://128.199.42.52:8080/

We enter the Docker container : 
root@ubuntu-s-2vcpu-4gb-ams3-01:~# docker exec -it 697d003f8985 bash
jenkins@697d003f8985:/$ 
// We are logged in as a Jenkins user

[
    {
        "CreatedAt": "2024-03-10T19:29:55Z",
        "Driver": "local",
        "Labels": null,
        "Mountpoint": "/var/lib/docker/volumes/jenkins_home/_data",
        "Name": "jenkins_home",
        "Options": null,
        "Scope": "local"
    }
]
## Connect to the server
(base) ➜ ssh root@128.199.42.52
root@ubuntu-s-2vcpu-4gb-ams3-01:~# 

### Access a docker container as a root user

root@ubuntu-s-2vcpu-4gb-ams3-01:~# docker exec -u 0 -it 697d003f8985 bash

root@697d003f8985:/# cat /etc/issue
Debian GNU/Linux 12 \n \l

To find the password
cat /var/lib/docker/volumes/jenkins_home/_data/secrets/initialAdminPassword

## Attach a volume to Jenkins from the host file
We need to have Docker command available in Jenkins = we need to attached a volume to Jenkins from the host files
For that we will mount Docker run time directory fron our droplet into the container as a volume
docker run -p 8080:8080 -p 50000:50000 -d \
-v jenkins_home:/var/jenkins_home \
-v /var/run/docker.sock:/var/run/docker.sock jenkins/jenkins:lts
** then log as a root user **

### fetch the latest version of docker and set the correct permission and install docker inside the jenkins container
curl https://get.docker.com/ > dockerinstall && chmod 777 dockerinstall && ./dockerinstall

## Change permission
root@c661f5db5013:/# ls -l /var/run/docker.sock
srw-rw---- 1 root 117 0 Mar 10 19:25 /var/run/docker.sock
root@c661f5db5013:/# chmod 666 /var/run/docker.sock
root@c661f5db5013:/# ls -l /var/run/docker.sock
srw-rw-rw- 1 root 117 0 Mar 10 19:25 /var/run/docker.sock
root@c661f5db5013:/# exit
## Log as a Jenkins user
root@ubuntu-s-2vcpu-4gb-ams3-01:~# docker exec -it c661f5db5013 bash
jenkins@c661f5db5013:/$ 
## Launch Docker command to test that I am allowed to execute docker command
jenkins@c661f5db5013:/$ docker pull redis

## groovy function
We name the file as the name of the function we call
With : #!/user/bin/env groovy 
At the beginning of the buildJar.groovy we add
 #!/user/bin/env groovy // in order to let know that we are working on a groovy file

 the remote path to test on Jenkins:
 https://gitlab.com/betty-godier/twn-devops-bootcamp/-/tree/main/002-build-automation-jenkins/jenkins-shared-library?ref_type=heads

 ## Make shared library globally available to use in a Jenkins file
Global Pipeline librairies create 'jenkins-shared-library'
=> we need to have versionning of our jenkins modification
=> we need to have a fix version