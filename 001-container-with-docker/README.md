# DevOps Practioner

## Portfolio

## Demonstrating How to Use Docker in Local Development Processes

Simply demo of an UI HTML, Javascript and nodejs application in the backend to simulate local developement process, connected to a docker container with a MongoDB in it.
![Demo of to use Docket in a local developement process](images/use-docker-local-development.jpeg "Use Docker for local development")


## Author

Betty Godier
[Linkedin Profile](https://www.linkedin.com/in/betty-godier/)

## Technologies used

JS, Nodejs
Docker, Docker Network
MongoDB container
MongoExpress container
NodeJs container


## Docker command
docker images
docker network ls
docker ps # check running container
docker ps -a # list all container on disk 
docker network create mongo-network
docker run mongo
docker stop <container-id> # stop the container
docker rm <container-id> # remove the container
docker container prune # suppress all stop conteneurs
docker network inspect mongo-network # check network config
docker logs c9fe92d195ce | tail # to see the last part of the logs
docker logs c9fe92d195ce -f # to see the changes without lauching logs again

### start mongo container 
[Mongodb specifications](https://hub.docker.com/_/mongo)
docker run -d \ # start in detach mode
-p 27017:27017 \ # open a port of mongoDB, default port of mongoDB is 27017
-e MONGO_INITDB_ROOT_USERNAME=admin \ # e is environnement variable 
-e MONGO_INITDB_ROOT_PASSWORD=password \ # we give the username and password, we want to use in the starter process
--name mongodb \ # overwrite the name of the container
--net mongo-network  # this container run in the mongo network
mongo # start the container

docker logs 648a1373ff4a1a867398c337f14b384df57372b6e4072b7656571f8bccd54854 # see whether it was successful and see what's happening inside

### start mongo express container and connect to the mongodb container
[Mongo express specifications](https://hub.docker.com/_/mongo-express)
we want mongo express to connect to the running mongodb container on start up
docker run -d \
-p 8081:8081 \ 
-e ME_CONFIG_MONGODB_ADMINUSERNAME=admin \
-e ME_CONFIG_MONGODB_ADMINPASSWORD=password \
--net mongo-network \
--name mongo-express \
-e ME_CONFIG_MONGODB_SERVER=mongodb  \ # docker ps to check the name of the container
mongo-express # the name of the image

## connect nodejs to the database
We have the Mongodb container and the Mongoexpress containers running.
To connect NodeJS to the Database : the url for a Mongodb database is the local and the port is accessible at.

## Docker compose

All this commands will be structures in a file : Docker compose called 'mongo.yaml' , that will take care of creating a common network for this container

## Build our own Docker Image
Ready to deploy -> need to be package in a Docker Container
We will build a Docker image from our application (Javascript - NodeJS) and prepare it to be deploy on some environment
We will mimic what Jenkins do : Builds JS App & creates image (and push in s Docker Repository.)
A Dockerfile is a blueprint for building images
FROM node \\ the image that we will start from 'install node'

docker build -t app:1.0 . \\ give a name and a tag, the dot mean the current folder
docker images \\ to see the image created

## Access the container though the command line terminal
docker ps \\ to have the container ID
docker exec -it 9b09950f860f /bin/bash \\ in bash
docker exec -it 9b09950f860f /bin/sh \\ in shell

we are in the root directory : /home/app # 
env \\ to see details on the environement