# DevOps Practioner



## Portfolio

List of demo projects that I built throughout the DevOps Bootcamp.

## Techworld with Nana devops bootcamp

### Cloud & Infrastructure as Service
- [X] Demo project : create server and deploy application on DigitalOcean

### Artifact Repository Manager with Nexus

- [ ] Demo project : Run Nexus on Droplet and Publish Artifact to Nexus

### Containers with Docker

- [X] Demo project : [Use Docker for local development](https://gitlab.com/betty-godier/twn-devops-bootcamp/-/tree/main/001-container-with-docker?ref_type=heads)
- [X] Demo project : Dockerize Nodejs application and push to private Docker registry
- [X] Demo project : Docker Compose - Run multiple Docker containers
- [X] Demo project : Deploy Docker application on a server with Docker Compose
- [X] Demo project : Persist data with Docker Volumes
- [X] Demo project : Deploy Nexus as Docker container
- [X] Demo project : Create Docker repository on Nexus and push to it

### Build automation & CI/CD with Jenkins

- [X] Demo project : Install Jenkins on DigitalOCean
- [ ] Demo project : Create a Jenkins Shared Library
- [ ] Demo project : Create a CI Pipeli=ne with Jenkinsfile ( Freestyle, Pipeline, Multibranch Pipeline)
- [ ] Demo project : Configure Webhook to trigger CIPipeline automatically on every change 
- [ ] Demo project : Dynamically Increment Application version in Jenkins Pipeline

### AWS Services

- [ ] Demo project : Deploy Web Application on EC2 instance (manually)
- [ ] Demo project : CD-Deploy Application from Jenkins Pipeline on EC2 Instance (automatically with docker-compose)
- [ ] Demo project : CD-Deploy Application from Jenkins Pipeline to EC2 Instance (automatically with docker)
- [ ] Demo project : Complete the CI/CD Pipeline (Docker-Compose, Dynamic versioning)
- [ ] Demo project : Interacting with AWS CLI

### Container Orchestration with Kubernetes

- [ ] Demo project : Deploy MongoDB and Mongo Express into local K8s cluster
- [ ] Demo project : Install a stateful service (MongoDB) on Kubernetes using Helm
- [ ] Demo project : Deploy Mosquitto message broker with ConfigMap and Secret Volume Types
- [ ] Demo project : Setup Prometheus Monitoring in Kubernetes cluster
- [ ] Demo project : Deploy Microservices application in Kubernetes with Production & Security Best Practices
- [ ] Demo project : Create Helm Chart for Microservices
- [ ] Demo project : Deploy Microservices with Helmfile

### Kubernetes on AWS - EKS

- [ ] Demo project : Create AWS EKS cluster with a Node Group
- [ ] Demo project : Create EKS cluster with Fargate profile
- [ ] Demo project : Create EKS cluster with eksctl
- [ ] Demo project : CD-Deploy to EKS cluster from Jenkins Pipeline
- [ ] Demo project : CD-Deploy to LKE cluster from Jenkins Pipeline
- [ ] Demo project : Complete CI/CD Pipeline with EKS and private DockerHub registry
- [ ] Demo project : Complete CI/CD Pipeline with EKS and AWS ECR

### Infrastructure as Code with Terraform

- [ ] Demo project : Automate AWS infrastructure
- [ ] Demo project : Modularize Project
- [ ] Demo project : Terraform & AWS EKS
- [ ] Demo project : Configure a Shared Remote State
- [ ] Demo project : Complete CI/CD with Terraform

### Programming with Python

- [ ] Demo project : Write Countdown Application
- [ ] Demo project : Automation with Python
- [ ] Demo project : API Request to GitLab

### Automation with Python

- [ ] Demo project : Health Check: EC2 Status Check
- [ ] Demo project : Automate configuring EC2 Server Instances
- [ ] Demo project : Automate displaying EKS cluster information
- [ ] Demo project : Data Backup & Restore
- [ ] Demo project : Website Monitoring and Recovery

### Configuration Management with Ansible

- [ ] Demo project : Automate Node.js application Deployment
- [ ] Demo project : Automate Nexus Deployment
- [ ] Demo project : Ansible & Docker
- [ ] Demo project : Ansible Integration in Terraform
- [ ] Demo project : Configure Dynamic Inventory
- [ ] Demo project : Automate Kubernetes Deployment
- [ ] Demo project : Andible Integration in Jenkins
- [ ] Demo project : Structure Playbooks with Ansible Roles

### Monitoring with Promotheus

- [ ] Demo project : Install Promotheus Stack in Kubernetes
- [ ] Demo project : Configure Alerting for our application
- [ ] Demo project : Configure Monitoring for a Third-Party Application
- [ ] Demo project : Configure Monitoring for Own Application

## Author

Betty Godier
[Linkedin Profile](https://www.linkedin.com/in/betty-godier/)


## Technologies used

DigitalOcean, Linode LKE, Linode
Amazon ECR, AWS, AWS EKS, AWS Fargate, AWS S3
Linux, Java, Gradle, Git
Nexus, Maven
Docker, Node.js, MongoDB, MongoExpress
Docker Compose
Jenkins, Groovy, GitLab
Docker Hub, Kubernetes, Mosquitto
K8s, Helm
Prometheus, Redis, Helmfile
Eksctl, Terraform
Python, IntelliJ, Boto3
Ansible, Linux, Grafana

## Design attribution
<a href="https://www.freepik.com/icon/settings_12219827#fromView=search&page=1&position=3&uuid=5c1f7113-81ba-4b83-bbd3-5f522b62ace5">Icon designed by afif fudin</a>